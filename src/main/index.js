import { app, BrowserWindow, ipcMain } from "electron";

import { Client } from "discord-rpc";
import fs from "fs";
import { homedir } from "os";
import path from "path";
import Hashids from "hashids";

const idHasher = new Hashids("sOwOce", 10);
const sharedPartyId = idHasher.encode("1");

const alphabet =
  "AZERTYUIOPQSDFGHJKLMWXCVBNazertyuiopqsdfghjklmwxcvbn1234567890-";

const makeId = (length = 8) =>
  Array(length)
    .fill()
    .map(() => alphabet[Math.floor(Math.random() * (alphabet.length - 1))])
    .join("");
var rpc = new Client({ transport: "ipc" });

const loadPresences = () => {
  if (!fs.existsSync(path.join(homedir(), ".presences"))) {
    fs.writeFileSync(
      path.join(homedir(), ".presences"),
      JSON.stringify([
        {
          _id: "xmVxLWTF",
          clientId: "553960708828692500",
          details: "ur mom vegan",
          largeImageKey: "richardo",
          largeImageText: "Can't deny you know him",
          name: "Richardo",
          state: "MADE YOU CLICK"
        }
      ])
    );
  }
  return JSON.parse(
    fs.readFileSync(path.join(homedir(), ".presences"), () => {})
  );
};

const refreshPresence = presence => {
  rpc.destroy();

  rpc = new Client({ transport: "ipc" });

  rpc.on("ready", () => {
    const activity = {
      state: presence.state,
      details: presence.details,
      partySize: presence.partySize,
      partyMax: presence.partyMax,
      instance: false
    };

    const parameters = [
      "smallImageKey",
      "smallImageText",
      "largeImageKey",
      "largeImageText"
    ];

    for (let param of parameters) {
      if (param in presence && presence[param] !== "") {
        activity[param] = presence[param];
      }
    }

    if (presence.timestamp === true) {
      activity.startTimestamp = Math.floor(new Date().getTime() / 1000);
    }

    if (presence.partyId === true) {
      activity.partyId = presence.partyId === true ? sharedPartyId : null;
    }

    if (presence.spectateSecret === true) {
      activity.spectateSecret =
        presence.spectateSecret === true ? makeId() : null;
    }

    if (presence.joinSecret === true) {
      activity.joinSecret = presence.joinSecret === true ? makeId() : null;
    }

    if (
      "partySize" in presence &&
      "partyMax" in presence &&
      presence.partySize != "" &&
      presence.partyMax != ""
    ) {
      activity.partySize = parseInt(presence.partySize);
      activity.partyMax = parseInt(presence.partyMax);
    }

    return rpc
      .setActivity(activity)
      .catch(err => console.error(`[Discord Presence]: ${err}`))
      .then(console.log);
  });

  rpc.login({ clientId: presence.clientId }).catch(console.error);
};

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== "development") {
  global.__static = require("path")
    .join(__dirname, "/static")
    .replace(/\\/g, "\\\\");
}

let mainWindow;
const winURL =
  process.env.NODE_ENV === "development"
    ? `http://localhost:9080`
    : `file://${__dirname}/index.html`;

function createWindow() {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000
  });

  mainWindow.loadURL(winURL);

  mainWindow.on("closed", () => {
    mainWindow = null;
  });
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on("get-presences-list", e => {
  e.returnValue = loadPresences();
});

ipcMain.on("set-presence-id", (e, activity) => {
  refreshPresence(activity);
});

ipcMain.on("get-new-id", e => {
  e.returnValue = makeId();
});

ipcMain.on("update-presences", (e, presences) => {
  fs.writeFileSync(
    path.join(homedir(), ".presences"),
    JSON.stringify(presences)
  );
});
